# Copyright 2022-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - César López Ramírez - <cesar.lopez@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Calendar iCal Enhacements",
    "version": "12.0.1.0.0",
    "depends": ["calendar"],
    "author": "Coopdevs Treball SCCL",
    "category": "Extra Tools",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        Enhances iCal export of Odoo Calender Event
    """,
    "installable": True,
}

