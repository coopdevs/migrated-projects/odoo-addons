# Copyright 2021-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - César López Ramírez - <cesar.lopez@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Product Standard Margin in Kanban view",
    "version": "12.0.1.0.1",
    "depends": ["product_standard_margin"],
    "author": "Coopdevs Treball SCCL",
    "category": "Sales",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        Print Product Standard Margin Rate in Kanban view
    """,
    "data": ["views/product_views.xml"],
    "installable": True,
}
