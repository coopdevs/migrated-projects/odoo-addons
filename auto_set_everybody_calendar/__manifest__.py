# Copyright 2021-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - César López Ramírez - <cesar.lopez@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Auto Set Everybody's Calendar in Calendar View",
    "version": "12.0.1.0.2",
    "depends": ["calendar","web"],
    "author": "Coopdevs Treball SCCL",
    "category": "Accounting & Finance",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        In Calendar main view auto toggles checkboxes (Everything, Everybody)
    """,
    "data": ['views/assets.xml'],
    "installable": True,
}
